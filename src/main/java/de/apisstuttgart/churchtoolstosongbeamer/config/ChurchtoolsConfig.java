package de.apisstuttgart.churchtoolstosongbeamer.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("churchtools")
@Getter
@Setter
@CommonsLog
public class ChurchtoolsConfig {

    protected String instance;
    protected String userId;
    protected String userToken;

    public String getUrlBase() {
        if (instance == null || instance.isEmpty()) {
            log.error("Please set the churchtools instance");
            return "";
        }

        if (instance.contains(".")) {
            return "https://" + instance + "/index.php";
        }

        return "https://" + instance + ".church.tools/index.php";
    }
}
