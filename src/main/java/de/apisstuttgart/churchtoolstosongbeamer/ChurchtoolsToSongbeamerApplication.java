package de.apisstuttgart.churchtoolstosongbeamer;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtEvent;
import de.apisstuttgart.churchtoolstosongbeamer.service.AgendaConverterService;
import de.apisstuttgart.churchtoolstosongbeamer.service.CtEventService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

import javax.swing.*;
import java.awt.*;

@SpringBootApplication
@CommonsLog
public class ChurchtoolsToSongbeamerApplication {

    @Autowired
    protected AgendaConverterService agendaConverterService;

    @Autowired
    protected CtEventService ctEventService;

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    protected static Container contentPanel;

    public static void main(String[] args) {
        new SpringApplicationBuilder(ChurchtoolsToSongbeamerApplication.class).headless(false).run(args);
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {
            log.debug("Debug logging is active.");

            initGUI();

            // Check config
            if (churchtoolsConfig.getUrlBase().isEmpty()) {
                showMessage("Bitte Churchtools Instanz richtig konfigurieren.");
            } else if (churchtoolsConfig.getUserId().isEmpty()) {
                showMessage("Bitte die User ID richtig konfigurieren.");
            } else if (churchtoolsConfig.getUserToken().isEmpty()) {
                showMessage("Bitte die User Token richtig konfigurieren.");

                // Config seems correct
            } else {
                CtEvent[] ctEvents = ctEventService.getAllEvents();
                if (ctEvents != null) {
                    showEventList(ctEvents, agendaConverterService);
                } else {
                    showMessage("Daten konnten nicht geladen werden.");
                }
            }
        };
    }


    protected static void initGUI() {
        JFrame jFrame = new JFrame("Churchtools to Songbeamer");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPanel = jFrame.getContentPane();
        contentPanel.setLayout(new BorderLayout());

        jFrame.setSize(400, 400);
        jFrame.setVisible(true);

        showMessage("Bitte warten, die Daten werden geladen.");
    }


    protected static void showMessage(String message) {
        contentPanel.removeAll();

        JLabel jLabel = new JLabel(message);

        contentPanel.add(jLabel, BorderLayout.NORTH);
        contentPanel.revalidate();
        contentPanel.repaint();
    }


    protected static void showEventList(CtEvent[] ctEvents, AgendaConverterService agendaConverterService) {
        showMessage("Agenda auswählen um diese im SongBeamer zu öffnen.");

        JList list = new JList(ctEvents);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(selectionEvent -> {
            if (!selectionEvent.getValueIsAdjusting()) {
                log.info("Selected object for translation is: " + list.getSelectedValue());
                agendaConverterService.translate((CtEvent) list.getSelectedValue());
            }
        });

        contentPanel.add(list);
        contentPanel.revalidate();
        contentPanel.repaint();
    }
}
