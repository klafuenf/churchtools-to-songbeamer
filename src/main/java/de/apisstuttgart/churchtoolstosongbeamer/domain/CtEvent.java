package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
public class CtEvent implements Comparable {

    protected String id;

    @JsonProperty("startdate")
    protected String start;

    @JsonProperty("valid_yn")
    protected String valid;

    @JsonProperty("bezeichnung")
    protected String name;

    @JsonProperty("category_id")
    protected String categoryId;

    protected Boolean agenda;

    protected String notizen;
    protected String ort;
    protected String link;

    public LocalDateTime getStartAsDateTime() {
        return LocalDateTime.parse(this.start.replaceFirst(" ", "T"));
    }

    public boolean isPast() {
        return LocalDate.now().isAfter(getStartAsDateTime().toLocalDate());
    }

    @Override
    public int compareTo(Object otherObject) {
        if (otherObject instanceof CtEvent) {
            LocalDateTime thisStart = this.getStartAsDateTime();
            LocalDateTime otherStart = ((CtEvent) otherObject).getStartAsDateTime();

            return thisStart.compareTo(otherStart);
        }
        return 0;
    }

    @Override
    public String toString() {
        return start + " - " + name;
    }
}
