package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.config.SongbeamerConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtEvent;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtResponse;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@CommonsLog
public class CtEventService {

    @Autowired
    protected CtLoginService ctLoginService;

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    @Autowired
    protected SongbeamerConfig songbeamerConfig;

    protected RestTemplate restTemplate = new RestTemplate();


    public CtEvent[] getAllEvents() {
        String requestParams = "?q=churchservice/ajax&func=getAllEventData";

        HttpEntity requestEntity = new HttpEntity(ctLoginService.getRequestHeader());

        ResponseEntity<CtResponse<Map<Long, CtEvent>>> response = restTemplate.exchange(churchtoolsConfig.getUrlBase() + requestParams, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<CtResponse<Map<Long, CtEvent>>>() {
        });

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Successful event lookup");

            Collection<CtEvent> eventCollection = response.getBody().getData().values();

            // Filter past events and events without an agenda
            eventCollection = eventCollection.stream().filter(ctEvent -> !ctEvent.isPast()).filter(ctEvent -> Boolean.TRUE.equals(ctEvent.getAgenda())).collect(Collectors.toList());

            CtEvent[] events = eventCollection.toArray(new CtEvent[0]);

            Arrays.sort(events);

            log.debug("Events: " + Arrays.toString(events));

            return events;
        }

        log.error("Login was not possible, please check your instance, id and token.");

        return null;
    }
}
