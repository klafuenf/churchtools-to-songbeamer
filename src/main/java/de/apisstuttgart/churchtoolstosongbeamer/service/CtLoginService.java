package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtResponse;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service
@CommonsLog
public class CtLoginService {

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    protected RestTemplate restTemplate = new RestTemplate();

    protected static HttpHeaders requestHeader = new HttpHeaders();


    /**
     * Login with id and token from the settings
     */
    protected void performLogin() {

        String requestParams = "?q=login/ajax"
                + "&func=loginWithToken"
                + "&id=" + churchtoolsConfig.getUserId()
                + "&token=" + churchtoolsConfig.getUserToken();

        ResponseEntity<CtResponse> response = restTemplate.postForEntity(churchtoolsConfig.getUrlBase() + requestParams, null, CtResponse.class);

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Login was successful :)");

            List<String> cookies = response.getHeaders().get(HttpHeaders.SET_COOKIE);

            StringBuilder cookieBuilder = new StringBuilder();

            cookies.forEach(cookie -> {
                cookieBuilder.append(cookie.replaceFirst(";.*$", "; "));
            });

            requestHeader.set(HttpHeaders.COOKIE, cookieBuilder.toString());
        } else {
            log.error("Login was not possible, please check your instance, id and token.");
        }
    }


    /**
     * Get request header which handles the authentication
     *
     * @return
     */
    public HttpHeaders getRequestHeader() {
        if (requestHeader.isEmpty()) {
            this.performLogin();
        }
        return requestHeader;
    }
}
