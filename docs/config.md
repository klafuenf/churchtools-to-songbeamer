# Konfiguration

Die Konfiguration wird in der Datei [`application.yml`](application.yml) festgelegt. Im folgenden ein Teil der möglichen Konfigurationen:

```
churchtools:
  instance: 'mittendrin'
  user-id: 12345
  user-token: 'in ChurchDB generierte Token hier eintragen'

songbeamer:
  install-path: 'C:\Program Files (x86)\SongBeamer'
  agenda-base-folder: 'C:\Users\Benutzername\Documents\SongBeamer'
  songs-base-folder: 'C:\Users\Benutzername\Documents\SongBeamer\Songs'

```

### Instance

Die Churchtools Instanz bezeichnet die Subdomain von Churchtools, also den ersten Teil (vor `.church.tools`).
Selfhoster geben einfach ihre Domain an, bspw. `churchtools.meine-gemeinde.de`.
Wichtig: Es wird eine Transportverschlüsselung mit TLS vorausgesetzt.

### User-ID

Findet man in ChurchDB rechts unten beim entsprechenden Benutzer.

![Screenshot ChurchDB](img/user-id.png)


### Token

Um einen Token zu erstellen, in ChurchDB den Benutzer wählen.
Dort auf `Berechtigungen` klicken.
Im Popup auf `Login-Token` klicken und den Login-Token kopieren.

### Install Path

Der Pfad unter dem der SongBeamer installiert ist.

### Agenda Base Folder

Hier werden die ausgewählten Abläufe abgespeichert.

### Song Base Folder

Hier wird der in `songs-sub-folder` definierte Ordner angelegt und darin alle Songs gespeichert, welche somit im Songbeamer zur Verfügung stehen.
